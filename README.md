# cardinal

StarCraft: BW bot on Java

## Описание запуска:

1. Установить BWAPI v4.1.2 (https://github.com/bwapi/bwapi/releases/tag/v4.1.2).
     Необходима оригинальная игра SC:BW с патчем 1.16.1. Можно взять с ICCUP (http://files.theabyss.ru/sc/starcraft.zip).
   
2. Скомпилировать исходный код в jar при помощи IDE Eclipse.

3. В папке с BWAPI найти и запустить ChaosLauncher (или Chaoslauncher - Multiinstanse для одновременного запуска нескольких сессий игры)

4. Включить плагины: BWAPI 4.1.2 Injector \[RELEASE\] и W-MODE.

5. Зайти в настройки инджектора и указать **api = NULL**

6. Запустить бота и игру.

7. .....

8. PROFIT!