import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import bwapi.Position;

public class DiscoveryWay {
	private List<Position> positions;
	private int key;
	private boolean cycled;
	
	DiscoveryWay(List<Position> pos, boolean cyc) {
		positions = pos;
		key = 0; 
		cycled = cyc;
	}
	
	DiscoveryWay(List<Position> pos) {
		this(pos, false);
	}
	
	public boolean isWayPoint(Position pos) {
		Position currPoint = positions.get(key);
		double dist = pos.getDistance(currPoint);
		if (dist < 100) {
			return true;
		} else {
			return false;
		}
	}
	
	public Position next() {
		if (key + 1 >= positions.size()) {
			return null;
		} else {
			key++;
			return positions.get(key);
		}
	}
	
	public Position current() {
		if (key >= positions.size()) {
			return null;
		} else {
			return positions.get(key);
		}
	}
}