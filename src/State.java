

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import bwapi.Game;
import bwapi.Mirror;
import bwapi.Player;
import bwapi.Position;
import bwapi.Unit;

public class State {
	
	public State() {
		mirror = new Mirror();
		workerCount = 0;
		commandCenters = new ArrayList<Unit>();
		enemyUnits = new TreeSet<Unit>();
		baraks = new ArrayList<Unit>();
		isAttack = false;
	}
	
	// Game class
    public Mirror mirror;
    public Game game;
    public Player self;
    
    
    public boolean isAttack;
    public Set<Unit> enemyUnits;
    public Unit scout; //Scout
    public DiscoveryWay discoveryPoints;
    
    public List<Unit> commandCenters;
    public int workerCount;
    public Unit builder;
    public List<Unit> baraks;
    
    
}
