import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

import bwapi.*;
import bwta.BWTA;
import bwta.BaseLocation;

public class Cardinal extends DefaultBWListener {

	State state;
    public Cardinal() {
    	state = new State();
    }
    
    public void run() {
        state.mirror.getModule().setEventListener(this);
        state.mirror.startGame();
    }


    
    @Override
    public void onUnitCreate(Unit unit) {
		UnitType unitType = unit.getType();
		if (unitType == UnitType.Terran_Command_Center) {
			state.commandCenters.add(unit);
		} else if (unitType.isWorker()) {
			state.workerCount++;
		} else if (unitType == UnitType.Terran_Barracks) {
			state.baraks.add(unit);
		} else if (unitType == UnitType.Terran_Marine) {
			state.scout = unit;
		}

        System.out.println("New unit discovered " + unitType);
    }

    @Override
    public void onUnitDestroy(Unit unit) {
    	Player player = unit.getPlayer();
    	UnitType type = unit.getType();
    	if (player == state.self) {
    		if (type == UnitType.Terran_Marine) {
    			state.baraks.get(0).train(UnitType.Terran_Marine);
    		}
    	} else {
    		state.isAttack = false;
    	}
    	System.out.println("Destroy " + type);
    }
    
    @Override
    public void onStart() {
        state.game = state.mirror.getGame();
        state.self = state.game.self();
        
        //Use BWTA to analyze map
        //This may take a few minutes if the map is processed first time!
        System.out.println("Analyzing map...");
        BWTA.readMap();
        BWTA.analyze();
        System.out.println("Map data ready");
        
        int i = 0;
        for(BaseLocation baseLocation : BWTA.getBaseLocations()){
        	System.out.println("Base location #" + (++i) + ". Printing location's region polygon:");
        	List<Position> region = baseLocation.getRegion().getPolygon().getPoints();
        	Position pred = region.get(0);
    		System.out.print(pred + ", ");
        	for(int j = 1; j < region.size(); j++) {
        		Position position = region.get(1);
        		state.game.drawLineMap(pred, position, Color.Red);
        		System.out.print(position + ", ");
        		pred = position;
        	}
        	System.out.println();
        }
    }

    @Override
    public void onFrame() {
    	
    	
        for(BaseLocation baseLocation : BWTA.getBaseLocations()){
        	List<Position> region = baseLocation.getRegion().getPolygon().getPoints();
        	Position pred = region.get(0);
        	for(int j = 1; j < region.size(); j++) {
        		Position position = region.get(j);
        		state.game.drawLineMap(pred, position, Color.Red);
        		pred = position;
        	}
        }

        //game.setTextSize(10);
        state.game.drawTextScreen(10, 10, "Playing as " + state.self.getName() + " - " + state.self.getRace());

        StringBuilder units = new StringBuilder("My units:\n");

        if (state.baraks.size() > 0
        		&& state.baraks.get(0) != null 
        		&& state.self.minerals() >= 50 
        		&& state.scout == null 
        		&& state.baraks.get(0).isIdle()) {
        	state.baraks.get(0).train(UnitType.Terran_Marine);
        }
        if (state.self.minerals() >= 50 && state.workerCount < 8) {
    		state.commandCenters.get(0).train(UnitType.Terran_SCV);
    	}
        
        if (state.scout != null) {
			if (!state.isAttack) {
				if (state.discoveryPoints == null) {

					List<Position> positions = new ArrayList<Position>();
					for (BaseLocation bl : BWTA.getStartLocations()) {
						positions.add(bl.getPosition());
					}
					Comparator<Position> posComp = new Comparator<Position>() {
						public int compare(Position a1, Position a2) {
							return a1.getApproxDistance(a2);
						}
					};
					positions.sort(posComp);
					state.discoveryPoints = new DiscoveryWay(positions);
				} else {
					Position wayPosition;
					if (state.discoveryPoints.isWayPoint(state.scout.getPosition())) {
						wayPosition = state.discoveryPoints.next();
					} else {
						wayPosition = state.discoveryPoints.current();
					}
					if (wayPosition != null) {
						state.scout.move(wayPosition);
					}

				}

				if (state.game.enemies().get(0).getUnits().size() > 0) {
					Player enemPlayer = state.game.enemies().get(0);
					ArrayList<Unit> enemyUnits = (ArrayList<Unit>) enemPlayer.getUnits(); // new ArrayList<Unit>();
					Comparator<Unit> unitComp = new Comparator<Unit>() {
						public int compare(Unit a1, Unit a2) {
							return a1.getDistance(a2);
						}
					};
					enemyUnits.sort(unitComp);
					for (Unit u : enemyUnits) {
						if (state.scout.canAttack(u) && !u.getType().isBuilding()) {
							state.isAttack = true;
							state.scout.attack(u);
							break;
						}
					}
				}
			}
        }
        
        if (state.builder != null && state.builder.isIdle()) {
    		state.builder = null;
    	} 
        
        //iterate through my units
        for (Unit myUnit : state.self.getUnits()) {
        	
            units.append(myUnit.getType()).append(" ").append(myUnit.getTilePosition()).append("\n");

            UnitType unitType = myUnit.getType();
            //if there's enough minerals, train an SCV
            if (unitType.isWorker() && myUnit != state.builder) {
            	
            	if (state.self.minerals() >= 150 && state.builder == null && state.baraks.size() < 1) {
            		state.builder = myUnit;
            		TilePosition buildPos = findNearBuildPosition(UnitType.Terran_Barracks);
            		if (buildPos != null) {
            			state.builder.build(UnitType.Terran_Barracks, buildPos);
            		}	
            	} else if (myUnit.isIdle()) {
            		//if it's a worker and it's idle, send it to the closest mineral patch
            		Unit closestMineral = null;

					// find the closest mineral
					for (Unit neutralUnit : state.game.neutral().getUnits()) {
						if (neutralUnit.getType().isMineralField()) {
							if (closestMineral == null
									|| myUnit.getDistance(neutralUnit) < myUnit.getDistance(closestMineral)) {
								closestMineral = neutralUnit;
							}
						}
					}

					// if a mineral patch was found, send the worker to gather it
					if (closestMineral != null) {
						myUnit.gather(closestMineral, false);
					}
            	}
            }
        }

        //draw my units on screen
        state.game.drawTextScreen(10, 25, units.toString());
    }

    private TilePosition findNearBuildPosition(UnitType build) {
		TilePosition comPos =  state.commandCenters.get(0).getTilePosition();
		for (int i = 0; i < 50; i++) {
			for (int j = 0; j < 50; j++) {
				int dx = (int) (Math.pow(-1, i)*((int)(i/2)));
				int dy = (int) (Math.pow(-1, j)*((int)(j/2)));
				TilePosition testPos = new TilePosition(comPos.getX()+dx, comPos.getY()+dy);
				if (state.game.canBuildHere(testPos, build)) {
					return testPos;
				}
			}
		}
		return null;
	}

	public static void main(String[] args) {
        new Cardinal().run();
    }
}